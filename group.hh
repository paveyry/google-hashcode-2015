#ifndef GROUP_HH
#define GROUP_HH

#include <set>
#include "server.hh"

class Group
{
  public:
    Group(int grp_num) : num(grp_num), servers(std::set<Server*>()) {}
    ~Group() = default;

    int get_num() { return num; }

    void add_server(Server* s)
    {
      servers.insert(s);
      s->set_group(this);
    }

    int get_tot_capacity()
    {
      int tot = 0;
      for (Server* s : servers)
        tot += s->get_capacity();
      return tot;
    }

    // Get the capacity with the given row disabled
    int get_garantized_capacity(std::vector<Row*> rows)
    {
      int res = -1;
      for (Row* disabled : rows)
      {
        int tot = 0;
        for (Server* s : servers)
        {
          if (s->get_row() != disabled)
            tot += s->get_capacity();
        }
        if (res == -1 || tot < res)
          res = tot;
      }
      return res;
    }

  private:
    int num;
    std::set<Server*> servers;
};

#endif
