CC = clang++-3.5
CXXFLAGS = -Wall -Wextra -pedantic -std=c++1y -g3 -ggdb
SRC = main.cc
OBJ = $(SRC:.cc=.o)
BIN = hash

all: $(BIN)

$(BIN): $(OBJ)
	$(LINK.c) $(OUTPUT_OPTION) $(OBJ)

export: clean
	rm -f submission.zip
	zip submission.zip *


clean:
	rm -f $(OBJ)
	rm -f $(BIN)
