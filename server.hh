#ifndef SERVER_HH
#define SERVER_HH

class Row;
class Group;

class Row;

class Server
{
  public:
      Server(int s, int c) : size(s), capacity(c) {}

      int get_size() { return size; }
      void set_size(int s) { size = s; }
      int get_capacity() { return capacity; }
      void set_capacity(int c) { capacity = c; }
      float get_power() { return (float)size / capacity; }

      int get_pos() { return pos; }
      Row* get_row() { return row; }
      void set_position(Row* r, int p) { row = r; pos = p; }

      Group* get_group() { return grp; }
      void set_group(Group* g) { grp = g; }

  private:
    int size;
    int capacity;
    int pos = -1;
    Row* row = nullptr;
    Group* grp = nullptr;
};

#endif
