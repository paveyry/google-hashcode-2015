#ifndef ROW_HH
# define ROW_HH

# include <vector>
# include <list>
# include "server.hh"

class Server;

class Row
{
  public:
    Row(std::vector<int>& unavail, int size, int row)
      : serv_(size)
      , avail_(size)
      , size_{size}
      , row_{row}
    {
      for (int i = 0; i < size; i++)
      {
        avail_[i] = 0;
        serv_[i] = nullptr;
      }
      for (auto i = unavail.begin(); i != unavail.end(); i++)
        avail_[*i] = 2;
    }

    bool add_old(Server* s)
    {
      int j;
      for (int i = 0; i < size_; i++)
      {
        for (j = i; j < size_ && avail_[j] == 0; j++)
          continue;
        if (j - i >= s->get_size())
        {
          serv_[i] = s;
          s->set_position(this, i);
          for (int k = 0; k < s->get_size(); k++)
            avail_[i + k] = 1;
          return true;
        }
      }
      return false;
    }

    bool add(Server* s)
    {
      int pos = get_best_pos(s->get_size()).first;
      if (pos == -1)
        return false;
      for (int i = pos; i < size_ && i < pos + s->get_size(); ++i)
      {
        avail_[i] = 1;
        serv_[i] = s;
      }
      s->set_position(this, pos);
      return true;
    }

    int max(int servsize)
    {
      int i = get_best_pos(servsize).second;
      if (i == -1)
        return size_ + 2;
      return i;
    }

    std::pair<int, int> get_best_pos(int servsize)
    {
      int j;
      int i = 0;
      std::list<std::pair<int, int>> lst;
      for (i = 0; i < size_; i++)
      {
        for (j = i; j < size_ && avail_[j] == 0; j++)
          continue;
        lst.push_back(std::pair<int, int>(i, j-i));
        i = j;
      }
      lst.sort([](std::pair<int, int> un, std::pair<int, int> deux)
          {
            return un.second < deux.second;
          });
      for (auto o = lst.begin(); o != lst.end(); o++)
      {
        if (o->second >= servsize)
          return *o;
      }
      return std::pair<int, int>(-1, -1);
    }

    int get_row() { return row_;}

  private:
    std::vector<Server*> serv_;
    std::vector<int> avail_;
    int size_;
    int row_;
};

#endif /* end of include guard: ROW_HH */
