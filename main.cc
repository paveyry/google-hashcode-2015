#include <iostream>
#include <fstream>

#include "input.hh"
#include "output.hh"

int main()
{
  Input in{std::cin};

  std::fstream out;
  out.open("res.out", std::fstream::out | std::fstream::trunc);
  Output(out, in);
  out.close();
  return 0;
}
