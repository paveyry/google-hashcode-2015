#ifndef INPUT_HH
# define INPUT_HH

# include <fstream>
# include <vector>
# include <algorithm>
# include "row.hh"
# include "server.hh"
# include "group.hh"

class Input
{
  public:
    Input(std::istream& stream)
    {
      int rows, slots, closedSlots, nb_groups, servers;
      stream >> rows >> slots >> closedSlots >> nb_groups >> servers;

      for (int i = 0; i < nb_groups; ++i)
        groups.push_back(new Group(i));

      std::vector<std::vector<int>> closed;
      for (int i = 0; i < rows; ++i)
        closed.push_back(std::vector<int>{});

      for (int i = 0; i < closedSlots; ++i)
      {
        int row, slot;
        stream >> row >> slot;
        closed[row].push_back(slot);
      }
      for (int i = 0; i < servers; ++i)
      {
        int size, capacity;
        stream >> size >> capacity;
        servs.push_back(new Server(size, capacity));
      }

      std::vector<Server*> servs_;
      for (auto s : servs)
        servs_.push_back(s);
      std::sort(servs_.begin(), servs_.end(), [](Server* s, Server* s2)
          {
            return s->get_power() < s2->get_power();
          });

      doCoolThings(closed, servs_, slots);
    }

    void doCoolThings(std::vector<std::vector<int>> closed, std::vector<Server*> servers, int slots)
    {
      std::vector<Row> map;
      for (int i = 0; i < (int)closed.size(); ++i)
        map.push_back(Row{closed[i], slots, i});
      //std::random_shuffle(map.begin(), map.end());
      unsigned i = 0;
      for (auto s : servers)
      {
        Row* row = nullptr;
        int max = slots + 1;
        for (Row& r : map)
        {
          int m = r.max(s->get_size());
          if (m <= max)
          {
            max = m;
            row = &r;
          }
        }
        if (row && row->add(s))
          groups[(i++) % groups.size()]->add_server(s);
      }
    }

    ~Input() = default;

    std::vector<Row*> rows = std::vector<Row*>();
    std::vector<Server*> servs = std::vector<Server*>();
    std::vector<Group*> groups = std::vector<Group*>();
};

#endif /* !INPUT_HH */
