#ifndef OUTPUT_HH
#define OUTPUT_HH

#include <iostream>
#include <fstream>
#include <vector>

class Output
{
  public:
    Output(std::ostream& ostr, Input& i)
    {
      for (Server* s : i.servs)
      {
        if (!s->get_row() || !s->get_group())
          ostr << "x";
        else
          ostr << s->get_row()->get_row() << " " << s->get_pos() << " "
               << s->get_group()->get_num();
        ostr << std::endl;
      }
    }
};

#endif
